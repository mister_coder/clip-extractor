#include <chrono>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

struct VideoSegment {
    double startSec;
    double endSec;
    std::string videoFilePath;
};

std::string videoFileLocation;
int killCount = 0;
double startSec;
double endSec;
int preKillDuration = 0; // Duration before kill to start segment
int postKillDuration = 5; // Duration after kill to end segment
int maxKillGap = 13; // Max time between kills to merge into one segment
int maxSegmentLength = 59; // Max length of a segment
std::vector<VideoSegment> videoSegments; // List of all segment start and end times
cv::Vec3b killColorThreshold = cv::Vec3b(230, 230, 35); // Color threshold to detect a kill

void parseConfig(const std::string& configFilePath) {
    std::ifstream configFile(configFilePath);
    if (!configFile.is_open()) {
        std::cerr << "Unable to open config file: " << configFilePath << std::endl;
        return;
    }
    std::string line;
    while (std::getline(configFile, line)) {
        std::istringstream is_line(line);
        std::string key;
        if (std::getline(is_line, key, '=')) {
            std::string value;
            if (std::getline(is_line, value)) {
                if (key == "startSec") startSec = std::stod(value);
                else if (key == "endSec") endSec = std::stod(value);
                else if (key == "videoFileLocation") videoFileLocation = value;
                else if (key == "preKillDuration") preKillDuration = std::stoi(value);
                else if (key == "postKillDuration") postKillDuration = std::stoi(value);
                else if (key == "killColorThreshold") {
                    std::vector<int> bgr;
                    std::stringstream ss(value);
                    std::string item;
                    while (std::getline(ss, item, ',')) {
                        bgr.push_back(std::stoi(item));
                    }
                    if (bgr.size() == 3) {
                        killColorThreshold = cv::Vec3b(bgr[0], bgr[1], bgr[2]);
                    } else {
                        std::cerr << "Invalid killColorThreshold format in config. Expected format: B,G,R" << std::endl;
                    }
                }
                else if (key == "maxSegmentLength") maxSegmentLength = std::stoi(value);
                else if (key == "maxKillGap") maxKillGap = std::stoi(value);
            }
        }
    }
}

std::string formatTime(double timeInSeconds) {
    int hours = int(timeInSeconds) / 3600;
    int minutes = (int(timeInSeconds) % 3600) / 60;
    int seconds = int(timeInSeconds) % 60;

    std::stringstream ss;
    ss << std::setw(2) << std::setfill('0') << hours << ":"
       << std::setw(2) << std::setfill('0') << minutes << ":"
       << std::setw(2) << std::setfill('0') << seconds;

    return ss.str();
}

bool isBluePixelDetected(const cv::Mat& frame) {
    cv::Vec3b pixelColor;
    int frameWidth = frame.cols;
    int frameHeight = frame.rows;
    if (frameWidth == 1920 && frameHeight == 1080) {
        pixelColor = frame.at<cv::Vec3b>(cv::Point(1830, 40));
    } else if (frameWidth == 2560 && frameHeight == 1440) {
        pixelColor = frame.at<cv::Vec3b>(cv::Point(2439, 49));
    } else {
        std::cerr << "Unsupported resolution: " << frameWidth << "x" << frameHeight << std::endl;
        return false;
    }

    return pixelColor[0] > killColorThreshold[0] && pixelColor[1] > killColorThreshold[1] && pixelColor[2] < killColorThreshold[2]; 
}

// Due to the nature of video keyframes, start and end times might not be precise
void saveSegmentUsingFFmpeg(VideoSegment segment) {
    std::stringstream ssStart, ssDuration;
    ssStart << std::fixed << std::setprecision(3) << segment.startSec;
    ssDuration << std::fixed << std::setprecision(3) << (segment.endSec - segment.startSec);

    std::filesystem::path filePath(segment.videoFilePath);
    std::string filenameWithoutExtension = filePath.stem().string();
    std::string outputFolderPath = filePath.parent_path().string() + "/";
    std::string outputFileName = outputFolderPath + filenameWithoutExtension + "_kill_" + std::to_string(killCount) + ".mp4";
    std::string ffmpegCmd = "ffmpeg -benchmark -hwaccel cuda -y -ss " + ssStart.str() + " -i \"" + segment.videoFilePath + "\" -t " + ssDuration.str() + " -map 0:v -map 0:a -c:v h264_nvenc -preset fast -cq 19 -c:a copy \"" + outputFileName + "\"";

    std::cout << "Executing: " << ffmpegCmd << std::endl;
    int ret = system(ffmpegCmd.c_str());
    if (ret != 0) {
        std::cerr << "FFmpeg command failed with return code " << ret << std::endl;
    } else {
        std::cout << "Saved segment: " << outputFileName << std::endl;
    }
}

void saveSegmentUsingOpenCV(VideoSegment segment) {
    cv::VideoCapture videoCapture(segment.videoFilePath);
    if (!videoCapture.isOpened()) {
        std::cerr << "Error opening video file for segment saving." << std::endl;
        return;
    }

    double fps = videoCapture.get(cv::CAP_PROP_FPS);
    int startFrame = static_cast<int>(fps * segment.startSec);
    int endFrame = static_cast<int>(fps * segment.endSec);

    std::string filenameWithoutExtension = std::filesystem::path(segment.videoFilePath).stem().string();
    std::string outputFileName = filenameWithoutExtension + "_kill_" + std::to_string(killCount) + ".mp4";
    cv::VideoWriter videoWriter(outputFileName, cv::VideoWriter::fourcc('M', 'P', '4', 'V'), fps, cv::Size(videoCapture.get(cv::CAP_PROP_FRAME_WIDTH), videoCapture.get(cv::CAP_PROP_FRAME_HEIGHT)));

    if (!videoWriter.isOpened()) {
        std::cerr << "Error opening video writer." << std::endl;
        return;
    }

    videoCapture.set(cv::CAP_PROP_POS_FRAMES, startFrame);

    cv::Mat frame;
    for (int i = startFrame; i <= endFrame && videoCapture.read(frame); ++i) {
        videoWriter.write(frame);
    }

    videoCapture.release();
    videoWriter.release();

    std::cout << "Saved segment: " << outputFileName << std::endl;
}

void saveSegmentTimes(const std::string& videoFilePath, double segmentStartSec, double segmentEndSec) {
    if (!videoSegments.empty()) {
        VideoSegment& lastSegment = videoSegments.back();
        double totalSegmentLength = segmentEndSec - lastSegment.startSec;
        double timeBetweenKills = segmentStartSec - lastSegment.endSec;

        if (totalSegmentLength <= maxSegmentLength && timeBetweenKills <= maxKillGap) {
            lastSegment.endSec = segmentEndSec;
            std::cout << "Merging segment ending at " << formatTime(lastSegment.endSec) << std::endl;
            return;
        }
    }
    
    VideoSegment newSegment{segmentStartSec, segmentEndSec, videoFilePath};
    videoSegments.push_back(newSegment);
    std::cout << "Adding new segment starting at " << formatTime(newSegment.startSec) << std::endl;
}

int main() {
    parseConfig("config.ini");

    cv::VideoCapture videoCapture(videoFileLocation);
    if (!videoCapture.isOpened()) {
        std::cerr << "Error opening video file." << std::endl;
        return -1;
    }

    videoCapture.set(cv::CAP_PROP_POS_MSEC, startSec * 1000);

    const int gracePeriodFrames = 120;
    bool bluePixelDetectedLastFrame = false;
    int framesSinceLastKill = gracePeriodFrames; 
    cv::Mat frame;
    while (videoCapture.read(frame)) {
        if (framesSinceLastKill < gracePeriodFrames) {
            framesSinceLastKill++;
            continue;
        }

        if (isBluePixelDetected(frame)) {
            cv::imshow("Frame", frame);
            if (cv::waitKey(1) == 27) break;
            killCount++;
            double currentTimestamp = videoCapture.get(cv::CAP_PROP_POS_MSEC) / 1000.0;
            std::cout << "Kill detected at " << formatTime(currentTimestamp) << " Kill " << killCount << std::endl;
            saveSegmentTimes(videoFileLocation, std::max(0.0, currentTimestamp - preKillDuration), currentTimestamp + postKillDuration);
            framesSinceLastKill = 0;
        }

    }

    std::cout << "Total Kill Count: " << killCount << std::endl;
    videoCapture.release();
    cv::destroyAllWindows();

    killCount = 0;
    for (const auto& segment : videoSegments) {
        killCount++;
        saveSegmentUsingFFmpeg(segment);
    }

    return 0;
}