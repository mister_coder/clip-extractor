🎯 Kill Detection & Video Clipping
This tool automates the detection of in-game kills in gameplay videos and extracts highlight clips.

🔹 Features
- 🖼 Frame Processing: Uses OpenCV to analyze video frames.
- 🎬 Video Editing: Uses FFmpeg for high-quality encoding.
- 🎯 Kill Detection: Identifies kills based on color thresholds (configured in config.ini).
- ⏳ Timestamping & Clipping: Marks kill events and segments video accordingly.
- 🔗 Merging: Combines nearby kills into a single clip if within a set interval.

🛠 How It Works
- 1️⃣ Processes video frames to detect kills.
- 2️⃣ Generates timestamps for each detected kill.
- 3️⃣ Extracts video segments centered around each event.
- 4️⃣ Merges clips when kills happen close together.
- 5️⃣ Saves high-quality output clips.


Designed for content creators and gaming communities looking to automate clip extraction. 🎮